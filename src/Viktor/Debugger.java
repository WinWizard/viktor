/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Debugger.java
 *
 * Description: For turning off debug code
 */

package Viktor;
//allows debug code to be disabled
public class Debugger {

    boolean debugEnabled = false;

    public Debugger(boolean debugEnabled) {
        this.debugEnabled = debugEnabled;
    }

    public void setDebugEnabled(boolean enable) {
        debugEnabled = enable;
    }

    public void print(String text) {
        if (debugEnabled) {
            System.out.print(text);
        }
    }

    public void println(String text) {
        if (debugEnabled) {
            System.out.println(text);
        }
    }

}
