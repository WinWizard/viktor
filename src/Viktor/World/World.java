/* 
 * AP(r) Computer Science GridWorld Case Study:
 * Copyright(c) 2005-2006 Cay S. Horstmann (http://horstmann.com)
 *
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * @author Cay Horstmann
 */
package Viktor.World;

import info.gridworld.grid.BoundedGrid;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import java.awt.GridLayout;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.JPanel;

/**
 *
 * A <code>World</code> is the mediator between a grid and the GridWorld GUI.
 * <br />
 * This class is not tested on the AP CS A and AB exams.
 */
public class World<T> extends JPanel {

    private Grid<T> gr;
    private Set<String> occupantClassNames;
    private Set<String> gridClassNames;
    private String message;
    private JPanel panel;
    private static Random generator = new Random();

    private static final int DEFAULT_ROWS = 100;
    private static final int DEFAULT_COLS = 100;
    private static final int DEFAULT_LAYS = 3;

    public World() {
        this(new BoundedGrid<T>(DEFAULT_ROWS, DEFAULT_COLS, DEFAULT_LAYS));
        message = null;
        setLayout(new GridLayout(30, 40));

    }

    public World(Grid<T> g) {
        gr = g;
        gridClassNames = new TreeSet<String>();
        occupantClassNames = new TreeSet<String>();
    }

    /**
     * Constructs and shows a frame for this world.
     */
    /**
     * Gets the grid managed by this world.
     *
     * @return the grid
     */
    public Grid<T> getGrid() {
        return gr;
    }

    /**
     * Sets the grid managed by this world.
     *
     * @param newGrid the new grid
     */
    public void setGrid(Grid<T> newGrid) {
        gr = newGrid;
        repaint();
    }

    /**
     * Sets the message to be displayed in the world frame above the grid.
     *
     * @param newMessage the new message
     */
    public void setMessage(String newMessage) {
        message = newMessage;
        repaint();
    }

    /**
     * Gets the message to be displayed in the world frame above the grid.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * This method is called when the user clicks on the step button, or when
     * run mode has been activated by clicking the run button.
     */
    public void step() {
        repaint();
    }

    /**
     * This method is called when the user clicks on a location in the
     * WorldFrame.
     *
     * @param loc the grid location that the user selected
     * @return true if the world consumes the click, or false if the GUI should
     * invoke the Location->Edit menu action
     */
    public boolean locationClicked(int button, Location loc) {
        return false;
    }

    /**
     * Adds an occupant at a given location.
     *
     * @param loc the location
     * @param occupant the occupant to add
     */
    public void addOccupant(Location loc, T occupant) {
        getGrid().put(loc, occupant);
        repaint();
    }

    /**
     * Removes an occupant from a given location.
     *
     * @param loc the location
     * @return the removed occupant, or null if the location was empty
     */
    public T removeOccupant(Location loc) {
        T r = getGrid().remove(loc);
        repaint();
        return r;
    }

    /**
     * Returns a string that shows the positions of the grid occupants.
     */
    public String toString() {
        String s = "";
        Grid<?> gr = getGrid();

        int rmin = 0;
        int rmax = gr.getNumRows() - 1;
        int cmin = 0;
        int cmax = gr.getNumCols() - 1;
        int lmin = 0;
        int lmax = gr.getNumLays() - 1;
        if (rmax < 0 || cmax < 0 || lmax < 0) // unbounded grid
        {
            for (Location loc : gr.getOccupiedLocations()) {
                int r = loc.getRow();
                int c = loc.getCol();
                int l = loc.getLay();
                if (r < rmin) {
                    rmin = r;
                }
                if (r > rmax) {
                    rmax = r;
                }
                if (c < cmin) {
                    cmin = c;
                }
                if (c > cmax) {
                    cmax = c;
                }
                if (l < lmin) {
                    lmin = c;
                }
                if (l > lmax) {
                    lmax = c;
                }
            }
        }
        for (int k = lmin; k <= lmax; k++) {
            for (int i = rmin; i <= rmax; i++) {
                for (int j = cmin; j < cmax; j++) {
                    Object obj = gr.get(new Location(i, j, k));
                    if (obj == null) {
                        s += " ";
                    } else {
                        s += obj.toString().substring(0, 1);
                    }
                }
                s += "\n";
            }
        }
        return s;
    }

}
