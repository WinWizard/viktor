/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: ControlPane.java
 *
 * Description: A panel for descriptions of all the controls in the game
 */
package Viktor.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class ControlPane extends JPanel {

    JTextArea textArea;

    ControlPane() {
        String OS = System.getProperty("os.name").toLowerCase();
        int cols = 57, rows = 40;
        if (OS.indexOf("win") >= 0) {
            cols = 63;
            rows = 40;
        } else if (OS.indexOf("mac") >= 0) {
            cols = 52;
            rows = 40;
        } else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") >= 0) {
            cols = 57;
            rows = 40;
        }
        setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 10, 10), new EtchedBorder()));
        this.setBackground(Color.DARK_GRAY);
        setLayout(new GridLayout(1, 1));

        textArea = new JTextArea(7, cols);
        textArea.setBorder(new EmptyBorder(0, 0, 0, 0));
        textArea.setForeground(Color.white);
        textArea.setFont(this.getFont().deriveFont(Font.BOLD));
        textArea.setBackground(Color.BLACK);
        textArea.setEditable(false);
        add(textArea);

    }

    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);

        if (aFlag == true) {
            textArea.setText(""
                    + "Default Controls: \n"
                    + "\n"
                    + "Movement is controlled with the arrow keys\n"
                    + "\n"                    
                    + "'i' opens the players inventory inside the inventory\n"
                    + "     you can click items to equip them\n\n"
                    + ""
                    + "',' picks up the item beneath the player's feet\n\n"
                    + ""
                    + "Move into enemies to attack them and Doors to open them\n\n"
                    + ""
                    + "'o' opens doors\n\n"
                    + ""
                    + "'c' closes doors\n\n"
                    + ""
                    + "'d' to drop items (TBI)\n\n"
                    + ""
                    + "';' will pass the turn\n\n"
                    + ""
                    + "'h' will pass turns until you are healed to max\n\n"
                    + ""
                    + "'q' to quit game\n\n");
            //enter controls here
        }

    }

}
