/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: MainFrame.java
 *
 * Description: Main jFrame, holds all the other stuff for the mostpart, except player data
 */
package Viktor.GUI;

import Viktor.GUI.Controls.ControlListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MainFrame extends JPanel implements ActionListener {

    public static Console console;

    public static GameWindow window;
    InfoBox info;
    Timer timer;
    public static Inventory inv;
    public static ControlPane cPane;
    
    public static ControlListener listener;

    public MainFrame() {
        console = new Console();
        window = new GameWindow();
        inv = new Inventory();
        cPane = new ControlPane();
         listener = new ControlListener();
        
        setBackground(Color.DARK_GRAY);
        

        
        this.addKeyListener(listener);

        setFocusable(true);

        
        setLayout(new BorderLayout());
        
        info = new InfoBox();
        add(window, BorderLayout.CENTER);
        add(info, BorderLayout.EAST);
        add(console, BorderLayout.SOUTH);
        timer = new Timer(5, this);
        timer.start();

    }

    public void dispose() {
        listener.writeConfig();

    }

    public InfoBox getInfo(){
        return info;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        window.repaint();
    }

}
