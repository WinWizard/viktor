/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Console.java
 *
 * Description: Text window at bottom of frame giving helpful descriptions of what's going on
 */
package Viktor.GUI;

import com.selesse.tailerswift.gui.SmartScroller;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class Console extends JPanel {

    JTextArea textArea;
    JScrollPane textPanel;

    public Console() {
        //set os specific defaults
        String OS = System.getProperty("os.name").toLowerCase();
        int cols = 57;
        if (OS.indexOf("win") >= 0) {
            cols = 63;
        } else if (OS.indexOf("mac") >= 0) {
            cols = 52;
        } else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") >= 0) {
            cols = 57;
        }
        textArea = new JTextArea(7, cols);

        setBackground(Color.DARK_GRAY);

        //this.setOpaque(false);
        setLayout(new BorderLayout());
        textArea.setLineWrap(true);

        textArea.setEditable(false);
        textArea.setBorder(new EmptyBorder(0, 0, 0, 0));
        textArea.setBackground(Color.BLACK);
        textPanel = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        new SmartScroller(textPanel, SmartScroller.VERTICAL, SmartScroller.END); //make the console auto scroll to bottom
        textPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(5, 10, 10, 10), new EtchedBorder()));
        textPanel.setBackground(Color.DARK_GRAY);
        textPanel.setOpaque(false);
        textArea.setForeground(Color.white);
        textArea.setFont(this.getFont().deriveFont(Font.BOLD));
        add(textPanel, BorderLayout.WEST);
        append("Press ? for a list of controls");

    }

    public void append(String append) {
        textArea.append(append + "\n");
    }
}
