/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: ControlListener.java
 *
 * Description: Listens for player inputs
 */
package Viktor.GUI.Controls;

import Viktor.Debugger;
import Viktor.RunGame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ControlListener extends KeyAdapter {

    Map<Integer, String> controls = new HashMap<>(); //map of controls without shift
    Map<Integer, String> controlsModified = new HashMap<>();//map of controls with shift

    Debugger debug = RunGame.debug;

    public ControlListener() {

        loadControls();//load the controls
    }

    public void loadControls() {

        try {

            readConfig();//read the config file

        } catch (FileNotFoundException | NumberFormatException ex) { //make sure input works
            Logger.getLogger(ControlListener.class.getName()).log(Level.SEVERE, "Error reading file, Using default config.", ex); //send console message
            controls.clear();
            controlsModified.clear();
            writeConfig();//if there is a config read error write the default values

        }

    }

    public void readConfig() throws FileNotFoundException, NumberFormatException {//reads the config 
        Scanner s = new Scanner(new FileReader(new File(/*this.getClass().getResource(".").getPath() + File.separator + */"controls.yml"))); //create a scanner for input
        for (int i = 0; s.hasNextLine();) { //loop through to input controls
            String input = s.nextLine();

            String[] temp = input.split(":");//split the string into its value and keycode

            if (temp.length == 2) {
                if (temp[1].contains("^")) {//check if the key is modified
                    temp[1] = temp[1].replace("^", "");//remove the modifier so that it can be parsed as an integer

                    controlsModified.put(Integer.parseInt(temp[1]), temp[0]); //try parsing the int and adding it to a map for the modified keys

                } else {
                    controls.put(Integer.parseInt(temp[1]), temp[0]); //put unmodified keys in the keymap
                }
                debug.println(temp[1]);

            }

        }

    }

    public void writeConfig() {//writes the config and initializes the default values if there are none stored
        if (controls.isEmpty()) { //reinitialize the control map if it is empty

            controls.put(38, "UP");
            controls.put(40, "DOWN");

            controls.put(37, "LEFT");
            controls.put(39, "RIGHT");
            controls.put(87, "WIELD_RIGHT");
            controls.put(65, "EQUIP_CHEST");
            controls.put(79, "OPEN");
            controls.put(67, "CLOSE"); //Redundant button which i was forced to add
            controls.put(44, "PICK_UP");
            controls.put(68, "DROP");
            controls.put(73, "INVENTORY");
            controls.put(81, "QUIT_S");
            controls.put(59, "PASS_TURN");
            controls.put(72, "HEAL");

        }

        if (controlsModified.isEmpty()) { //reinitialize the control map if it is empty
            controlsModified.put(81, "QUIT_NS");
            controlsModified.put(46, "GO_UP");
            controlsModified.put(44, "GO_DOWN");
            controlsModified.put(47, "QUERY");

        }
        /*Add Controls Here
         *use the format <controls.put(<action>,<keycode>);>
         */
        /*
         controls.put("UP", 0);
         controls.put("DOWN", 0);
         controls.put("Left", 0);
         */

        try {//try to write the file
            File f = new File(/*this.getClass().getResource(".").getPath() + File.separator + */"controls.yml");//create a new file object
            f.createNewFile();//create the config file
            FileWriter writer = new FileWriter(f);//create a writer

            Iterator entries = controls.entrySet().iterator(); //create an iterator to iterate through the map
            while (entries.hasNext()) { //loop until the end of entries is reached
                Entry entry = (Entry) entries.next(); //store the  value in the next iterator value to entry
                if ((Integer) entry.getKey() >= 0) {//if  the entry exists in the map then 
                    writer.write(entry.getValue() + ":" + entry.getKey()); //write the value and its keycode to the file
                } else {
                    writer.write(entry.getValue() + ":");//write the value to the file

                }
                //debug code 
                debug.println("" + entry.getKey());

                if (entries.hasNext()) {//add a newline unless it is the last entry
                    writer.write("\n");
                }

            }

            entries = controlsModified.entrySet().iterator(); // set up the modified controls iterator
            while (entries.hasNext()) {

                writer.write("\n");

                Entry entry = (Entry) entries.next();
                if ((Integer) entry.getKey() >= 0) {
                    writer.write(entry.getValue() + ":^" + entry.getKey());//write the modified key to the file
                } else {
                    writer.write(entry.getValue() + ":");

                }
                //debug code 
                debug.println("" + entry.getKey());

            }
            writer.flush();
            writer.close();

        } catch (IOException ex1) {
            Logger.getLogger(ControlListener.class.getName()).log(Level.SEVERE, "Error writing file. Config will not be saved.", ex1);
        }
    }

    public Map getControls() {
        return controls;
    }

    @Override
    public void keyPressed(KeyEvent e) { //key listener 
        if (e.isShiftDown()) { //check if the shift key is down
            debug.println("Shift + " + e.getKeyCode());

            if (controlsModified.containsKey(e.getKeyCode())) {//check if the keyCode is in the list
                RunGame.player.act(controlsModified.get(e.getKeyCode()));//send the keys command to player 
            }
        } else {
            debug.println("" + e.getKeyCode());
            if (controls.containsKey(e.getKeyCode())) {
                RunGame.player.act(controls.get(e.getKeyCode()));
            }
        }

    }

}
