/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Menu.java
 *
 * Description: The title menu to start the game
 */
package Viktor.GUI;

import Viktor.RunGame;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Menu extends JPanel implements ActionListener {

    JButton newGame;
    JButton cont;
    JButton quit;
    JPanel panel,panel1;
    JLabel title;
    public Menu() {
         try {
            
            if (this.getClass().getResource("ViktorTitle.png")!=null) {
                ImageIcon icon = new ImageIcon(ImageIO.read(this.getClass().getResource("ViktorTitle.png")));

                title = new JLabel(icon);

            } else {
                title = new JLabel("Viktor");
                title.setForeground(Color.RED);
                title.setFont(this.getFont().deriveFont(Font.BOLD, 70));
            }

        } catch (IOException ex) {

            title = new JLabel("Viktor");
            title.setForeground(Color.RED);
            title.setFont(this.getFont().deriveFont(Font.BOLD, 70));

            //Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
        panel1 = new JPanel(new GridLayout(2, 1));
        panel1.setBackground(Color.DARK_GRAY);
        title.setBackground(Color.gray);
                title.setBorder(new EmptyBorder(0, 0, 0, 0));

        setBackground(Color.DARK_GRAY);
        panel = new JPanel(new GridLayout(0, 1));
        panel.setBackground(Color.DARK_GRAY);
        newGame = new JButton();
        newGame.addActionListener(this);
        newGame.setBackground(Color.DARK_GRAY);
        newGame.setActionCommand("NEW_GAME");
        newGame.setForeground(Color.WHITE);
        newGame.setBorder(new EmptyBorder(0, 0, 0, 0));
        newGame.setFont(this.getFont().deriveFont(Font.BOLD));
        newGame.setText("New Game");
        cont = new JButton();
        cont.addActionListener(this);
        cont.setBackground(Color.DARK_GRAY);
        cont.setActionCommand("CONTINUE");
        cont.setForeground(Color.WHITE);
        cont.setBorder(new EmptyBorder(0, 0, 0, 0));
        cont.setFont(this.getFont().deriveFont(Font.BOLD));
        cont.setText("Continue Game");
        quit = new JButton();
        quit.addActionListener(this);
        quit.setBackground(Color.DARK_GRAY);
        quit.setActionCommand("QUIT");
        quit.setForeground(Color.WHITE);
        quit.setBorder(new EmptyBorder(0, 0, 0, 0));
        quit.setFont(this.getFont().deriveFont(Font.BOLD));
        quit.setText("Quit");
        
        panel1.add(title);
        panel.add(newGame);
        //panel.add(cont);
        panel.add(quit);
        panel1.add(panel);
        add(panel1);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        switch (cmd) {
            case "QUIT":
                QuitMenu q = new QuitMenu();
                q.setVisible(true);
                RunGame.frame1.add(q);
                setVisible(false);

                RunGame.frame1.repaint();
                RunGame.frame1.revalidate();
                break;
            case "NEW_GAME":
                NewGame g = new NewGame();
                g.setVisible(true);
                RunGame.frame1.add(g);
                setVisible(false);

                RunGame.frame1.repaint();
                RunGame.frame1.revalidate();
                break;

        }
    }
}
