/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: InfoBox.java
 *
 * Description: The map and the generation of it
 */
package Viktor.GUI;

import Viktor.Debugger;
import Viktor.Entity.Armor;
import Viktor.Entity.Door;
import Viktor.Entity.Duergar;
import Viktor.Entity.EntityWorld;
import Viktor.Entity.Floor;
import Viktor.Entity.Item;
import Viktor.Entity.Kobold;
import Viktor.Entity.MOB;
import Viktor.Entity.Minotaur;
import Viktor.Entity.Wall;
import Viktor.Entity.Weapon;
import Viktor.RunGame;
import Viktor.World.World;
import info.gridworld.grid.Location;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class GameWindow extends JPanel {

    public static ArrayList<MOB> MOBSOnFloor;
    JScrollPane scroll;
    public static EntityWorld gamePanel;
    //placeholder
    Debugger debug = RunGame.debug;

    public GameWindow() {
        gamePanel = new EntityWorld();
        MOBSOnFloor  = new ArrayList<>();
        setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 5, 0), new EtchedBorder()));
        setLayout(new BorderLayout());
        setBackground(Color.DARK_GRAY);

        this.add(gamePanel);
        this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        Door[] doorsOnFloor = {new Door(2, 13),
            new Door(4, 4),
            new Door(5, 16),
            new Door(6, 2), new Door(6, 22), new Door(6, 28),
            new Door(7, 10),
            new Door(9, 34),
            new Door(10, 6),
            new Door(11, 24),
            new Door(12, 11), new Door(12, 14),
            new Door(13, 18),
            new Door(14, 10), new Door(14, 26),
            new Door(15, 35),
            new Door(16, 12), new Door(16, 16),
            new Door(17, 2), new Door(17, 28),
            new Door(19, 25),
            new Door(21, 33),
            new Door(23, 8), new Door(23, 22),
            new Door(26, 19), new Door(26, 26), new Door(26, 31)};

        Wall[] wallsOnFloor = {new Wall(0, 13), new Wall(0, 14), new Wall(0, 15), new Wall(0, 16), new Wall(0, 17), new Wall(0, 18),
            new Wall(1, 7), new Wall(1, 8), new Wall(1, 9), new Wall(1, 10), new Wall(1, 11), new Wall(1, 12), new Wall(1, 13), new Wall(1, 18),
            new Wall(2, 0), new Wall(2, 1), new Wall(2, 2), new Wall(2, 3), new Wall(2, 4), new Wall(2, 7), new Wall(2, 18),
            new Wall(3, 0), new Wall(3, 4), new Wall(3, 5), new Wall(3, 6), new Wall(3, 7), new Wall(3, 9), new Wall(3, 10), new Wall(3, 11), new Wall(3, 12), new Wall(3, 13), new Wall(3, 18),
            new Wall(4, 0), new Wall(4, 11), new Wall(4, 13), new Wall(4, 18), new Wall(4, 22), new Wall(4, 23), new Wall(4, 24), new Wall(4, 25), new Wall(4, 26), new Wall(4, 27), new Wall(4, 28),
            new Wall(5, 0), new Wall(5, 4), new Wall(5, 5), new Wall(5, 6), new Wall(5, 7), new Wall(5, 8), new Wall(5, 9), new Wall(5, 11), new Wall(5, 13), new Wall(5, 14), new Wall(5, 15), new Wall(5, 17), new Wall(5, 18), new Wall(5, 19), new Wall(5, 20), new Wall(5, 21), new Wall(5, 22), new Wall(5, 28), new Wall(5, 29), new Wall(5, 30), new Wall(5, 31), new Wall(5, 32), new Wall(5, 33), new Wall(5, 34), new Wall(5, 35),
            new Wall(6, 0), new Wall(6, 1), new Wall(6, 3), new Wall(6, 4), new Wall(6, 9), new Wall(6, 11), new Wall(6, 15), new Wall(6, 35),
            new Wall(7, 1), new Wall(7, 3), new Wall(7, 6), new Wall(7, 7), new Wall(7, 8), new Wall(7, 9), new Wall(7, 11), new Wall(7, 15), new Wall(7, 16), new Wall(7, 17), new Wall(7, 18), new Wall(7, 19), new Wall(7, 20), new Wall(7, 21), new Wall(7, 22), new Wall(7, 28), new Wall(7, 29), new Wall(7, 30), new Wall(7, 31), new Wall(7, 32), new Wall(7, 33), new Wall(7, 35),
            new Wall(8, 1), new Wall(8, 3), new Wall(8, 6), new Wall(8, 11), new Wall(8, 14), new Wall(8, 18), new Wall(8, 15), new Wall(8, 16), new Wall(8, 17), new Wall(8, 22), new Wall(8, 28), new Wall(8, 33), new Wall(8, 35),
            new Wall(9, 1), new Wall(9, 3), new Wall(9, 4), new Wall(9, 5), new Wall(9, 6), new Wall(9, 11), new Wall(9, 14), new Wall(9, 18), new Wall(9, 22), new Wall(9, 28), new Wall(9, 32), new Wall(9, 33), new Wall(9, 35), new Wall(9, 36),
            new Wall(10, 1), new Wall(10, 11), new Wall(10, 14), new Wall(10, 18), new Wall(10, 22), new Wall(10, 28), new Wall(10, 32), new Wall(10, 36),
            new Wall(11, 1), new Wall(11, 3), new Wall(11, 4), new Wall(11, 5), new Wall(11, 6), new Wall(11, 11), new Wall(11, 12), new Wall(11, 13), new Wall(11, 14), new Wall(11, 18), new Wall(11, 22), new Wall(11, 23), new Wall(11, 25), new Wall(11, 26), new Wall(11, 27), new Wall(11, 28), new Wall(11, 32), new Wall(11, 36),
            new Wall(12, 1), new Wall(12, 3), new Wall(12, 6), new Wall(12, 18), new Wall(12, 19), new Wall(12, 20), new Wall(12, 21), new Wall(12, 22), new Wall(12, 23), new Wall(12, 25), new Wall(12, 26), new Wall(12, 27), new Wall(12, 32), new Wall(12, 36),
            new Wall(13, 1), new Wall(13, 3), new Wall(13, 6), new Wall(13, 11), new Wall(13, 12), new Wall(13, 13), new Wall(13, 14), new Wall(13, 27), new Wall(13, 32), new Wall(13, 36),
            new Wall(14, 1), new Wall(14, 3), new Wall(14, 6), new Wall(14, 7), new Wall(14, 8), new Wall(14, 9), new Wall(14, 11), new Wall(14, 12), new Wall(14, 13), new Wall(14, 14), new Wall(14, 18), new Wall(14, 19), new Wall(14, 20), new Wall(14, 21), new Wall(14, 22), new Wall(14, 23), new Wall(14, 24), new Wall(14, 25), new Wall(14, 27), new Wall(14, 28), new Wall(14, 32), new Wall(14, 36),
            new Wall(15, 1), new Wall(15, 3), new Wall(15, 9), new Wall(15, 13), new Wall(15, 14), new Wall(15, 18), new Wall(15, 23), new Wall(15, 28), new Wall(15, 32), new Wall(15, 33), new Wall(15, 34), new Wall(15, 36),
            new Wall(16, 1), new Wall(16, 3), new Wall(16, 9), new Wall(16, 10), new Wall(16, 11), new Wall(16, 13), new Wall(16, 14), new Wall(16, 15), new Wall(16, 17), new Wall(16, 18), new Wall(16, 23), new Wall(16, 28), new Wall(16, 29), new Wall(16, 30), new Wall(16, 31), new Wall(16, 32), new Wall(16, 33), new Wall(16, 34), new Wall(16, 36),
            new Wall(17, 1), new Wall(17, 3), new Wall(17, 4), new Wall(17, 5), new Wall(17, 6), new Wall(17, 7), new Wall(17, 8), new Wall(17, 9), new Wall(17, 14), new Wall(17, 15), new Wall(17, 17), new Wall(17, 23), new Wall(17, 36),
            new Wall(18, 1), new Wall(18, 8), new Wall(18, 9), new Wall(18, 14), new Wall(18, 15), new Wall(18, 17), new Wall(18, 23), new Wall(18, 28), new Wall(18, 29), new Wall(18, 30), new Wall(18, 31), new Wall(18, 32), new Wall(18, 34), new Wall(18, 35), new Wall(18, 36),
            new Wall(19, 1), new Wall(19, 8), new Wall(19, 9), new Wall(19, 14), new Wall(19, 15), new Wall(19, 17), new Wall(19, 18), new Wall(19, 19), new Wall(19, 20), new Wall(19, 21), new Wall(19, 22), new Wall(19, 23), new Wall(19, 24), new Wall(19, 26), new Wall(19, 27), new Wall(19, 28), new Wall(19, 32), new Wall(19, 34),
            new Wall(20, 1), new Wall(20, 8), new Wall(20, 9), new Wall(20, 10), new Wall(20, 11), new Wall(20, 12), new Wall(20, 13), new Wall(20, 14), new Wall(20, 15), new Wall(20, 17), new Wall(20, 18), new Wall(20, 26), new Wall(20, 32), new Wall(20, 34),
            new Wall(21, 1), new Wall(21, 8), new Wall(21, 10), new Wall(21, 17), new Wall(21, 18), new Wall(21, 20), new Wall(21, 21), new Wall(21, 22), new Wall(21, 23), new Wall(21, 24), new Wall(21, 25), new Wall(21, 26), new Wall(21, 31), new Wall(21, 32), new Wall(21, 34),
            new Wall(22, 1), new Wall(22, 8), new Wall(22, 9), new Wall(22, 10), new Wall(22, 12), new Wall(22, 13), new Wall(22, 14), new Wall(22, 15), new Wall(22, 16), new Wall(22, 17), new Wall(22, 18), new Wall(22, 23), new Wall(22, 31), new Wall(22, 34),
            new Wall(23, 1), new Wall(23, 12), new Wall(23, 18), new Wall(23, 19), new Wall(23, 20), new Wall(23, 21), new Wall(23, 23), new Wall(23, 24), new Wall(23, 25), new Wall(23, 26), new Wall(23, 31), new Wall(23, 34),
            new Wall(24, 1), new Wall(24, 8), new Wall(24, 9), new Wall(24, 10), new Wall(24, 12), new Wall(24, 19), new Wall(24, 26), new Wall(24, 31), new Wall(24, 34),
            new Wall(25, 1), new Wall(25, 8), new Wall(25, 10), new Wall(25, 12), new Wall(25, 13), new Wall(25, 14), new Wall(25, 15), new Wall(25, 16), new Wall(25, 17), new Wall(25, 18), new Wall(25, 19), new Wall(25, 26), new Wall(25, 27), new Wall(25, 28), new Wall(25, 29), new Wall(25, 30), new Wall(25, 31), new Wall(25, 34),
            new Wall(26, 1), new Wall(26, 2), new Wall(26, 3), new Wall(26, 4), new Wall(26, 5), new Wall(26, 6), new Wall(26, 7), new Wall(26, 8), new Wall(26, 10), new Wall(26, 34),
            new Wall(27, 10), new Wall(27, 11), new Wall(27, 12), new Wall(27, 13), new Wall(27, 14), new Wall(27, 15), new Wall(27, 16), new Wall(27, 17), new Wall(27, 18), new Wall(27, 19), new Wall(27, 20), new Wall(27, 21), new Wall(27, 22), new Wall(27, 23), new Wall(27, 24), new Wall(27, 25), new Wall(27, 26), new Wall(27, 27), new Wall(27, 28), new Wall(27, 29), new Wall(27, 30), new Wall(27, 31), new Wall(27, 32), new Wall(27, 33), new Wall(27, 34)};

        Floor[] floorsOnFloor = {new Floor(1, 14), new Floor(1, 15), new Floor(1, 16), new Floor(1, 17),
            new Floor(2, 8), new Floor(2, 9), new Floor(2, 10), new Floor(2, 11), new Floor(2, 12), new Floor(2, 14), new Floor(2, 15), new Floor(2, 16), new Floor(2, 17),
            new Floor(3, 1), new Floor(3, 2), new Floor(3, 3), new Floor(3, 8), new Floor(3, 14), new Floor(3, 15), new Floor(3, 16), new Floor(3, 17),
            new Floor(4, 1), new Floor(4, 2), new Floor(4, 3), new Floor(4, 5), new Floor(4, 6), new Floor(4, 7), new Floor(4, 8), new Floor(4, 9), new Floor(4, 10), new Floor(4, 14), new Floor(4, 15), new Floor(4, 16), new Floor(4, 17),
            new Floor(5, 1), new Floor(5, 2), new Floor(5, 3), new Floor(5, 10), new Floor(5, 23), new Floor(5, 24), new Floor(5, 25), new Floor(5, 26), new Floor(5, 27),
            new Floor(6, 10), new Floor(6, 16), new Floor(6, 17), new Floor(6, 18), new Floor(6, 19), new Floor(6, 20), new Floor(6, 21), new Floor(6, 23), new Floor(6, 24), new Floor(6, 25), new Floor(6, 26), new Floor(6, 27), new Floor(6, 29), new Floor(6, 30), new Floor(6, 31), new Floor(6, 32), new Floor(6, 33), new Floor(6, 34),
            new Floor(7, 2), new Floor(7, 23), new Floor(7, 24), new Floor(7, 25), new Floor(7, 26), new Floor(7, 27), new Floor(7, 34),
            new Floor(8, 2), new Floor(8, 7), new Floor(8, 8), new Floor(8, 9), new Floor(8, 10), new Floor(8, 23), new Floor(8, 24), new Floor(8, 25), new Floor(8, 26), new Floor(8, 27), new Floor(8, 34),
            new Floor(9, 2), new Floor(9, 7), new Floor(9, 8), new Floor(9, 9), new Floor(9, 10), new Floor(9, 15), new Floor(9, 16), new Floor(9, 17), new Floor(9, 23), new Floor(9, 24), new Floor(9, 25), new Floor(9, 26), new Floor(9, 27),
            new Floor(10, 2), new Floor(10, 3), new Floor(10, 4), new Floor(10, 5), new Floor(10, 7), new Floor(10, 8), new Floor(10, 9), new Floor(10, 10), new Floor(10, 15), new Floor(10, 16), new Floor(10, 17), new Floor(10, 23), new Floor(10, 24), new Floor(10, 25), new Floor(10, 26), new Floor(10, 27), new Floor(10, 33), new Floor(10, 34), new Floor(10, 35),
            new Floor(11, 2), new Floor(11, 7), new Floor(11, 8), new Floor(11, 9), new Floor(11, 10), new Floor(11, 15), new Floor(11, 16), new Floor(11, 17), new Floor(11, 33), new Floor(11, 34), new Floor(11, 35),
            new Floor(12, 2), new Floor(12, 7), new Floor(12, 8), new Floor(12, 9), new Floor(12, 10), new Floor(12, 12), new Floor(12, 13), new Floor(12, 15), new Floor(12, 16), new Floor(12, 17), new Floor(12, 24), new Floor(12, 33), new Floor(12, 34), new Floor(12, 35),
            new Floor(13, 2), new Floor(13, 7), new Floor(13, 8), new Floor(13, 9), new Floor(13, 10), new Floor(13, 15), new Floor(13, 16), new Floor(13, 17), new Floor(13, 19), new Floor(13, 20), new Floor(13, 21), new Floor(13, 22), new Floor(13, 23), new Floor(13, 24), new Floor(13, 25), new Floor(13, 26), new Floor(13, 33), new Floor(13, 34), new Floor(13, 35),
            new Floor(14, 2), new Floor(14, 15), new Floor(14, 16), new Floor(14, 17), new Floor(14, 33), new Floor(14, 34), new Floor(14, 35),
            new Floor(15, 2), new Floor(15, 10), new Floor(15, 11), new Floor(15, 12), new Floor(15, 15), new Floor(15, 16), new Floor(15, 17), new Floor(15, 24), new Floor(15, 25), new Floor(15, 26), new Floor(15, 27),
            new Floor(16, 2), new Floor(16, 24), new Floor(16, 25), new Floor(16, 26), new Floor(16, 27), new Floor(16, 35),
            new Floor(17, 10), new Floor(17, 11), new Floor(17, 12), new Floor(17, 13), new Floor(17, 16), new Floor(17, 24), new Floor(17, 25), new Floor(17, 26), new Floor(17, 27), new Floor(17, 29), new Floor(17, 30), new Floor(17, 31), new Floor(17, 32), new Floor(17, 33), new Floor(17, 34), new Floor(17, 35),
            new Floor(18, 2), new Floor(18, 3), new Floor(18, 4), new Floor(18, 5), new Floor(18, 6), new Floor(18, 7), new Floor(18, 10), new Floor(18, 11), new Floor(18, 12), new Floor(18, 13), new Floor(18, 16), new Floor(18, 24), new Floor(18, 25), new Floor(18, 26), new Floor(18, 27), new Floor(18, 33),
            new Floor(19, 2), new Floor(19, 3), new Floor(19, 4), new Floor(19, 5), new Floor(19, 6), new Floor(19, 7), new Floor(19, 10), new Floor(19, 11), new Floor(19, 12), new Floor(19, 13), new Floor(19, 16), new Floor(19, 33),
            new Floor(20, 2), new Floor(20, 3), new Floor(20, 4), new Floor(20, 5), new Floor(20, 6), new Floor(20, 7), new Floor(20, 16), new Floor(20, 19), new Floor(20, 20), new Floor(20, 21), new Floor(20, 22), new Floor(20, 23), new Floor(20, 24), new Floor(20, 25), new Floor(20, 33),
            new Floor(21, 2), new Floor(21, 3), new Floor(21, 4), new Floor(21, 5), new Floor(21, 6), new Floor(21, 7), new Floor(21, 11), new Floor(21, 12), new Floor(21, 13), new Floor(21, 14), new Floor(21, 15), new Floor(21, 16), new Floor(21, 19),
            new Floor(22, 2), new Floor(22, 3), new Floor(22, 4), new Floor(22, 5), new Floor(22, 6), new Floor(22, 7), new Floor(22, 11), new Floor(22, 19), new Floor(22, 20), new Floor(22, 21), new Floor(22, 22), new Floor(22, 32), new Floor(22, 33),
            new Floor(23, 2), new Floor(23, 3), new Floor(23, 4), new Floor(23, 5), new Floor(23, 6), new Floor(23, 7), new Floor(23, 9), new Floor(23, 10), new Floor(23, 11), new Floor(23, 32), new Floor(23, 33),
            new Floor(24, 2), new Floor(24, 3), new Floor(24, 4), new Floor(24, 5), new Floor(24, 6), new Floor(24, 7), new Floor(24, 11), new Floor(24, 20), new Floor(24, 21), new Floor(24, 22), new Floor(24, 23), new Floor(24, 24), new Floor(24, 25), new Floor(24, 32), new Floor(24, 33),
            new Floor(25, 2), new Floor(25, 3), new Floor(25, 4), new Floor(25, 5), new Floor(25, 6), new Floor(25, 7), new Floor(25, 11), new Floor(25, 20), new Floor(25, 21), new Floor(25, 22), new Floor(25, 23), new Floor(25, 24), new Floor(25, 25), new Floor(25, 32), new Floor(25, 33),
            new Floor(26, 11), new Floor(26, 12), new Floor(26, 13), new Floor(26, 14), new Floor(26, 15), new Floor(26, 16), new Floor(26, 17), new Floor(26, 18), new Floor(26, 20), new Floor(26, 21), new Floor(26, 22), new Floor(26, 23), new Floor(26, 24), new Floor(26, 25), new Floor(26, 27), new Floor(26, 28), new Floor(26, 29), new Floor(26, 30), new Floor(26, 32), new Floor(26, 33)};

        Item[] itemsOnFloor = {new Weapon("wooden", "sword", new Location(5, 3, 1)), new Weapon("steel", "mace", new Location(16,24,1)), new Weapon("steel", "axe", new Location(12, 7, 1)), new Weapon("mythril", "sword", new Location(11, 35, 1)),
                               new Armor("wooden", "chest", new Location(24, 4, 1)), new Armor("steel", "chest", new Location(9, 25, 1)), new Armor("mythril", "chest", new Location(25, 24, 1))};

        ArrayList<Floor> floorForMOBS = new ArrayList();
        for(int i=0;i<floorsOnFloor.length;i++){
            floorForMOBS.add(floorsOnFloor[i]);
        }
        
        //adds kobolds
        for(int i = 0, rnd = (int)(Math.random()*4+8);i < rnd;i++){
            int random = (int)(Math.random()*floorForMOBS.size());
            if(floorForMOBS.get(random).getLocation().getRow() < 3 || floorForMOBS.get(random).getLocation().getRow() > 5){
                if(floorForMOBS.get(random).getLocation().getCol() < 1 || floorForMOBS.get(random).getLocation().getCol() > 3){
                    MOBSOnFloor.add(new Kobold(floorForMOBS.get(random).getLocation().getRow(),floorForMOBS.get(random).getLocation().getCol()));
                    floorForMOBS.remove(random);
                }
                else{
                    i--;
                }
            }
            else{
                i--;
            }
        }    
        
        //adds duergar(s)
        for(int i = 0, rnd = (int)(Math.random()*1+1);i < rnd;i++){
            int random = (int)(Math.random()*floorForMOBS.size());
            if(floorForMOBS.get(random).getLocation().getRow() < 3 || floorForMOBS.get(random).getLocation().getRow() > 5){
                if(floorForMOBS.get(random).getLocation().getCol() < 1 || floorForMOBS.get(random).getLocation().getCol() > 3){
                    MOBSOnFloor.add(new Duergar(floorForMOBS.get(random).getLocation().getRow(),floorForMOBS.get(random).getLocation().getCol()));
                    floorForMOBS.remove(random);
                }
                else{
                    i--;
                }
            }
            else{
                i--;
            }
        }
        
        //adds a minotaur, maybe...
        for(int i = 0, rnd = (int)(Math.random()*2);i < rnd;i++){
            int random = (int)(Math.random()*floorForMOBS.size());
            if(floorForMOBS.get(random).getLocation().getRow() < 3 || floorForMOBS.get(random).getLocation().getRow() > 5){
                if(floorForMOBS.get(random).getLocation().getCol() < 1 || floorForMOBS.get(random).getLocation().getCol() > 3){
                    MOBSOnFloor.add(new Minotaur(floorForMOBS.get(random).getLocation().getRow(),floorForMOBS.get(random).getLocation().getCol()));
                    floorForMOBS.remove(random);
                }
                else{
                    i--;
                }
            }
            else{
                i--;
            }
        } 
        
        for (Floor item : floorsOnFloor) {
            gamePanel.addOccupant(item.getLocation(), item);
        }
        for (Door item : doorsOnFloor) {
            gamePanel.addOccupant(item.getLocation(), item);
        }
        for (Wall item : wallsOnFloor) {
            gamePanel.addOccupant(item.getLocation(), item);
        }
        for (Item item : itemsOnFloor) {
            gamePanel.addOccupant(item.getLocation(), item);
        }
        
        for (MOB item : MOBSOnFloor) {
            gamePanel.addOccupant(item.getLocation(), item);
        }

        gamePanel.addOccupant(new Location(4, 2, 0), RunGame.player);

        this.setVisible(true);

    }

    public World getWorld() {
        return gamePanel;
    }

    public static void actAll() {
        for(int i=0; i<MOBSOnFloor.size();i++){
            MOB m = MOBSOnFloor.get(i);
            m.act();
            if(m.isDead()){
                m.removeSelfFromGrid();
                MOBSOnFloor.remove(i);
                i--;
                GameWindow.gamePanel.repaint();
                GameWindow.gamePanel.revalidate();
            }
        }
    }
}
