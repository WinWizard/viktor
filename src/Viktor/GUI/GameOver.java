/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: GameOver.java
 *
 * Description: A panel displaying the game over window
 */
package Viktor.GUI;

import Viktor.RunGame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class GameOver extends JPanel implements ActionListener {

    JTextArea stats = new JTextArea(18, 12);
    String strength, dexterity, constitution, agility, currentHealth, maxHealth, armor, weapon, score;
    JPanel panel = new JPanel();
    JLabel over;
    JPanel p1 = new JPanel();
    JButton menu, quit;

    public GameOver() {
        update();
        try {
            if (this.getClass().getResource("GameOver.png") != null) {
                ImageIcon icon = new ImageIcon(ImageIO.read(this.getClass().getResource("GameOver.png")));

                over = new JLabel(icon);

            } else {
                over = new JLabel("Game Over");
                over.setForeground(Color.RED);
                over.setFont(this.getFont().deriveFont(Font.BOLD, 70));
            }

        } catch (IOException ex) {

            over = new JLabel("Game Over");
            over.setForeground(Color.RED);
            over.setFont(this.getFont().deriveFont(Font.BOLD, 70));

            //Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
        p1.setLayout(new BorderLayout());
        setFocusable(false);
        menu = new JButton();
        menu.addActionListener(this);
        menu.setBackground(Color.DARK_GRAY);
        setBackground(Color.DARK_GRAY);
        menu.setActionCommand("MENU");
        menu.setForeground(Color.WHITE);
        menu.setText("Menu");
        menu.setBorder(new EmptyBorder(0, 0, 0, 0));
        menu.setFont(this.getFont().deriveFont(Font.BOLD));
        quit = new JButton();
        quit.addActionListener(this);
        quit.setBackground(Color.DARK_GRAY);
        quit.setActionCommand("QUIT");
        quit.setForeground(Color.WHITE);
        quit.setText("Quit");
        quit.setBorder(new EmptyBorder(0, 0, 0, 0));
        quit.setFont(this.getFont().deriveFont(Font.BOLD));
        quit.setFocusable(false);
        menu.setFocusable(false);
        stats.setFocusable(false);
        panel.setFocusable(false);
        over.setFocusable(false);
        stats.setOpaque(false);
        stats.setLineWrap(true);
        stats.setEditable(false);
        stats.setFont(this.getFont().deriveFont(Font.BOLD));
        stats.setForeground(Color.white);
        panel.setLayout(new GridLayout(1, 2));
        panel.add(menu);
        panel.add(quit);
        p1.add(over, BorderLayout.NORTH);
        p1.add(panel, BorderLayout.SOUTH);
        p1.add(stats, BorderLayout.CENTER);
        add(p1);
        p1.setBackground(Color.DARK_GRAY);

    }

    public String setStats() {

        strength = RunGame.player.getStrength() + "";
        dexterity = RunGame.player.getDexterity() + "";
        constitution = RunGame.player.getConstitution() + "";
        agility = RunGame.player.getAgility() + "";
        currentHealth = RunGame.player.getCurrentHealth() + "";
        maxHealth = RunGame.player.getMaxHealth() + "";
        armor = RunGame.player.getArmor().toString();
        weapon = RunGame.player.getWeapon().toString();
        score = RunGame.score + "";

        return "Strength: " + strength
                + "\n\nDexterity: " + dexterity
                + "\n\nConstitution: " + constitution
                + "\n\nAgility: " + agility
                + "\n\nHealth: " + currentHealth + "/" + maxHealth
                + "\n\nArmor: " + armor
                + "\n\nWeapon:" + weapon
                + "\n\nScore: " + score;
    }

    public void update() {
        stats.setText(setStats());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        switch (cmd) {
            case "QUIT":
                QuitMenu q = new QuitMenu();
                q.setVisible(true);
                RunGame.frame1.add(q);
                setVisible(false);

                RunGame.frame1.repaint();
                RunGame.frame1.revalidate();

                break;
            case "MENU":
                RunGame.init();
                break;

        }
    }

}
