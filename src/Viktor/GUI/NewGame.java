/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: NewGame.java
 *
 * Description: Menu for starting a new game, (eg. naming your character)
 */
package Viktor.GUI;

import Viktor.RunGame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class NewGame extends JPanel implements ActionListener{
    JTextArea name = new JTextArea("Viktor",1,20);
    JButton accept;
    JButton cancel;
    JPanel panel,top;
    JLabel text;
    public NewGame(){
        panel = new JPanel(new BorderLayout());
        top = new JPanel(new GridLayout(2,1));
        text = new JLabel();
        text.setText("Enter your name");
        text.setForeground(Color.WHITE);
        text.setBorder(new EmptyBorder(0,0,0,0));
        text.setFont(this.getFont().deriveFont(Font.BOLD));
        text.setBackground(Color.DARK_GRAY);
        top.setBackground(Color.DARK_GRAY);
            panel.setBackground(Color.DARK_GRAY);
            setBackground(Color.DARK_GRAY);
        
        name.setForeground(Color.WHITE);
        name.setFont(this.getFont().deriveFont(Font.BOLD));
        name.setBackground(Color.BLACK);
        accept = new JButton();
        accept.addActionListener(this);
        accept.setBackground(Color.DARK_GRAY);
        accept.setActionCommand("ACCEPT");
        accept.setForeground(Color.WHITE);
        accept.setText("Accept");
        accept.setBorder(new EmptyBorder(0,0,0,0));
        accept.setFont(this.getFont().deriveFont(Font.BOLD));
        cancel = new JButton();
        cancel.addActionListener(this);
        cancel.setBackground(Color.DARK_GRAY);
        cancel.setActionCommand("CANCEL");
        cancel.setForeground(Color.WHITE);
        cancel.setText("Cancel");
        cancel.setBorder(new EmptyBorder(0,0,0,0));
        cancel.setFont(this.getFont().deriveFont(Font.BOLD));
        
        top.add(text);
        top.add(name);
        panel.add(top,BorderLayout.NORTH);
        panel.add(accept,BorderLayout.WEST);
        panel.add(cancel,BorderLayout.EAST);
        add(panel);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        switch(cmd){
            case "ACCEPT":
                    RunGame.player.setName(name.getText());
                    RunGame.frame1.add(RunGame.frame);
                    
                    RunGame.frame.setVisible(true);
                    setVisible(false);
                    RunGame.frame1.repaint();
                RunGame.frame1.revalidate();
                    
                break;
               case "CANCEL":
                   RunGame.frame1.add(RunGame.menu);
                    RunGame.menu.setVisible(true);
                    setVisible(false);
                    RunGame.frame1.repaint();
                RunGame.frame1.revalidate();
                   break;
                   
        }
    }

    
    
    
}
