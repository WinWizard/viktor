/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: QuitGame.java
 *
 * Description: Panel to quit the game
 */
package Viktor.GUI;

import Viktor.RunGame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class QuitGame extends JPanel implements ActionListener {
    

        JButton yes;
        JButton no;
        JLabel text;
        JPanel panel;

        public QuitGame(){
            panel = new JPanel(new BorderLayout());
            panel.setBackground(Color.DARK_GRAY);
            setBackground(Color.DARK_GRAY);
        text = new JLabel();
        text.setText("Would you like to quit?");
        text.setForeground(Color.WHITE);
        text.setBorder(new EmptyBorder(0,0,0,0));
        text.setFont(this.getFont().deriveFont(Font.BOLD));
        text.setBackground(Color.DARK_GRAY);
        yes = new JButton();
        yes.addActionListener(this);
        yes.setBackground(Color.DARK_GRAY);
        yes.setActionCommand("YES");
        yes.setForeground(Color.WHITE);
        yes.setText("Yes");
        yes.setBorder(new EmptyBorder(0,0,0,0));
        yes.setFont(this.getFont().deriveFont(Font.BOLD));
        no = new JButton();
        no.addActionListener(this);
        no.setBackground(Color.DARK_GRAY);
        no.setActionCommand("NO");
        no.setForeground(Color.WHITE);
        no.setText("No");
        no.setBorder(new EmptyBorder(0,0,0,0));
        no.setFont(this.getFont().deriveFont(Font.BOLD));
        panel.add(text,BorderLayout.NORTH);
        panel.add(yes,BorderLayout.WEST);
        panel.add(no,BorderLayout.EAST);
        add(panel);
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        switch(cmd){
            case "YES":
                RunGame.frame.dispose();
                System.exit(0);
                break;
            case "NO":
                    RunGame.frame1.add(RunGame.frame);
                    RunGame.frame.setVisible(true);
                    setVisible(false);
                    RunGame.frame1.repaint();
                RunGame.frame1.revalidate();
                break;
                    
        }
    }
}
