/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Inventory.java
 *
 * Description: A panel to show you what stuff you are lugging around, and holds it
 */
package Viktor.GUI;

import Viktor.Entity.Item;
import Viktor.RunGame;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

public class Inventory extends JPanel implements ActionListener {

    JPanel panel;
    JScrollPane pane;
    ArrayList<Item> inv;
    
    public Inventory() {
        setBackground(Color.DARK_GRAY);
        setLayout(new GridLayout(1, 1));
        setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 10, 5, 0), new EtchedBorder()));
        panel = new JPanel(new GridLayout(0, 3));
        //panel.setOpaque(false);
        panel.setBackground(Color.DARK_GRAY);
        pane = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pane.setBackground(Color.DARK_GRAY);
        add(pane);

        setVisible(false);

    }

    @Override
    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        if (aFlag == true) {
            panel.removeAll();
            panel.setFocusable(false);
            inv = RunGame.player.getInv();

            JButton button;
            int i=0;
            for (Item item : inv) {
                button = new JButton();
                button.setFocusable(false);
                button.setOpaque(false);
                button.addActionListener(this);
                button.setBackground(Color.DARK_GRAY);
                
                button.setActionCommand(i+"");
                
                button.setForeground(Color.white);
                button.setBorder(new EtchedBorder());
                button.setIcon(new ImageIcon(item.getImage()));
                button.setText(item.toString());
                panel.add(button);
                
                i++;
            }
            repaint();
            revalidate();

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        RunGame.player.equip(inv.get(Integer.parseInt(e.getActionCommand())));
    }

}
