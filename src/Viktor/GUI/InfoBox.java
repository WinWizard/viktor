/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: InfoBox.java
 *
 * Description: A side panel displaying player stats and current equipment
 */
package Viktor.GUI;

import Viktor.RunGame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class InfoBox extends JPanel {

    JTextArea stats = new JTextArea(18, 12);
    String strength, dexterity, constitution, agility, currentHealth, maxHealth, armor, weapon, score;

    public InfoBox() {
        setBackground(Color.DARK_GRAY);

        this.setBorder(new EmptyBorder(10, 5, 10, 10));
        stats.setOpaque(false);
        stats.setLineWrap(true);
        stats.setEditable(false);
        stats.setFont(this.getFont().deriveFont(Font.BOLD));
        stats.setForeground(Color.white);        
        this.setLayout(new BorderLayout());
        add(stats, BorderLayout.CENTER);
        
        update();
        
    }

    public String setStats() {

        strength = RunGame.player.getStrength() + "";
        dexterity = RunGame.player.getDexterity() + "";
        constitution = RunGame.player.getConstitution() + "";
        agility = RunGame.player.getAgility() + "";
        currentHealth = RunGame.player.getCurrentHealth() + "";
        maxHealth = RunGame.player.getMaxHealth() + "";
        armor = RunGame.player.getArmor().toString();
        weapon = RunGame.player.getWeapon().toString();
        score = RunGame.score + "";

        return     "Strength: "+ strength
                +"\n\nDexterity: "+dexterity
                +"\n\nConstitution: "+constitution
                +"\n\nAgility: "+agility
                +"\n\nHealth: "+currentHealth+"/"+maxHealth
                +"\n\nArmor: "+armor
                +"\n\nWeapon:"+weapon
                +"\n\nScore: "+score;
    }
    public void update(){
        stats.setText(setStats());
    }
}
