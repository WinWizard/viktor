/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: RunGame.java
 *
 * Description: Contains main, runs the game
 */
package Viktor;

import Viktor.Entity.Player;
import Viktor.GUI.MainFrame;
import Viktor.GUI.Menu;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class RunGame {

    public static Debugger debug = new Debugger(false);
    public static Player player;
    public static JFrame frame1;
    public static MainFrame frame;
    public static Menu menu;

    public static int score;

    public static void main(String[] args) {
        init();

    }

    public static void init() {
        if (frame1 != null) {
            frame1.dispose();
        }
        frame1 = new JFrame();

        score = 0;
        player = new Player();

        frame = new MainFrame();
        menu = new Menu();

        frame1.add(menu); //add the menu to the frame
        frame1.setTitle("Viktor"); //set the title of the game
        frame1.setSize(860, 660); //set the size to a constant size
        frame1.setResizable(false);
        frame1.setVisible(true);
        frame1.repaint();
        frame1.revalidate();

        frame1.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);//disable any action on close 
        frame1.addWindowListener(new WindowAdapter() {//enable writing on program close 
            @Override
            public void windowClosing(WindowEvent event) {
                int quit = 1;//add would you like to close
                if (quit == 1) {
                    frame.dispose();//write on program close
                    System.exit(0);//kill program
                }

            }
        });
    }

}
