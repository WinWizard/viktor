/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Entity.java
 *
 * Description: Anything that exists in the game world needs to extend entity
 */
package Viktor.Entity;

import Viktor.Debugger;
import Viktor.GUI.GameWindow;
import Viktor.RunGame;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Entity {

    private boolean isCollidable;
    private Location location;
    private BufferedImage icon;
    private Grid<Entity> grid;

    Debugger debug = RunGame.debug;
    private int direction;

    public Entity() {
        isCollidable = false;
        location = null;
        try {
            if (this.getClass().getResource(this.getClass().getSimpleName() + ".png") != null){
                            icon = ImageIO.read(this.getClass().getResource(this.getClass().getSimpleName() + ".png"));

            }
            else 
                icon = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        } catch (IOException ex) {
            //Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setLocation(Location loc) {
        this.location = loc;
    }

    public void setLocation(int row, int col, int lay) {
        this.location = new Location(row, col, lay);
    }

    public Location getLocation() {
        return location;
    }

    public void setCollision(boolean col) {
        isCollidable = col;
    }

    public boolean getCollision() {
        return isCollidable;
    }

    public void setImage(String imgName) {
        try {
            if (this.getClass().getResource(imgName + ".png") != null){
                            icon = ImageIO.read(this.getClass().getResource(imgName + ".png"));

            }
            else 
                icon = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        } catch (IOException ex) {
            //Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public BufferedImage getImage() {
        return icon;
    }

    public void act() {
        setDirection(getDirection() + Location.HALF_CIRCLE);
    }

    public void act(String k) {
    }

    public int getDirection() {
        return direction;
    }

    /**
     * Sets the current direction of this actor.
     *
     * @param newDirection the new direction. The direction of this actor is set
     * to the angle between 0 and 359 degrees that is equivalent to
     * <code>newDirection</code>.
     */
    public void setDirection(int newDirection) {
        direction = newDirection % Location.FULL_CIRCLE;
        if (direction < 0) {
            direction += Location.FULL_CIRCLE;
        }
    }

    /**
     * Gets the grid in which this actor is located.
     *
     * @return the grid of this actor, or <code>null</code> if this actor is not
     * contained in a grid
     */
    public Grid<Entity> getGrid() {
        return grid;
    }

    /**
     * Gets the location of this actor.
     *
     * @return the location of this actor, or <code>null</code> if this actor is
     * not contained in a grid
     */
    /**
     * Puts this actor into a grid. If there is another actor at the given
     * location, it is removed. <br />
     * Precondition: (1) This actor is not contained in a grid (2)
     * <code>loc</code> is valid in <code>gr</code>
     *
     * @param gr the grid into which this actor should be placed
     * @param loc the location into which the actor should be placed
     */
    public void putSelfInGrid(Grid<Entity> gr, Location loc) {
        if (grid != null) {
            throw new IllegalStateException(
                    "This actor is already contained in a grid.");
        }

        Entity entity = gr.get(loc);
        if (entity != null) {
            entity.removeSelfFromGrid();
        }
        gr.put(loc, this);
        grid = gr;
        this.location = loc;
    }

    /**
     * Removes this actor from its grid. <br />
     * Precondition: This actor is contained in a grid
     */
    public void removeSelfFromGrid() {
        if (grid == null) {
            throw new IllegalStateException(
                    "This actor is not contained in a grid.");
        }
        if (grid.get(location) != this) {
            throw new IllegalStateException(
                    "The grid contains a different actor at location "
                    + location + ".");
        }

        grid.remove(location);
        grid = null;
        location = null;

    }

    /**
     * Moves this actor to a new location. If there is another actor at the
     * given location, it is removed. <br />z Precondition: (1) This actor is
     * contained in a grid (2) <code>newLocation</code> is valid in the grid of
     * this actor
     *
     * @param newLocation the new location
     */
    public void moveTo(Location newLocation) {
        location.setLay(0);
        if (grid == null) {
            throw new IllegalStateException("This actor is not in a grid.");
        }
        if (grid.get(location) != this) {
            throw new IllegalStateException(
                    "The grid contains a different actor at location "
                    + location + ".");
        }
        if (!grid.isValid(newLocation)) {
            throw new IllegalArgumentException("Location " + newLocation
                    + " is not valid.");
        }

        if (newLocation.equals(location)) {
            return;
        }
        grid.remove(location);
        Entity other = grid.get(newLocation);
        if (other != null) {
            other.removeSelfFromGrid();
        }
        location = newLocation;
        grid.put(location, this);
        debug.println(grid.get(location).toString());
        GameWindow.gamePanel.repaint();
        GameWindow.gamePanel.revalidate();

    }
}
