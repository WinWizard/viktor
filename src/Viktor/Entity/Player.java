/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Player.java
 *
 * Description: Player character. also contains controls.
 */
package Viktor.Entity;

import Viktor.Debugger;
import Viktor.GUI.GameOver;
import Viktor.GUI.GameWindow;
import Viktor.GUI.MainFrame;
import Viktor.GUI.QuitGame;
import Viktor.RunGame;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import java.awt.BorderLayout;
import java.util.ArrayList;

public class Player extends Character {

    ArrayList<Item> inventory;
    Debugger debug = RunGame.debug;
    boolean open = false;
    boolean close = false;

    public Player() {
        inventory = new ArrayList<>();
        setName("Viktor");
        setWeapon(new Weapon("fleshy", "fist"));
        setArmor(new Armor("bare", "chest"));

    }

    @Override
    @SuppressWarnings("empty-statement")
    public void act(String k) {
        QuitGame q;
        Grid gr = getGrid();
        Location currentLocation = this.getLocation();
        if (open) {
            if (gr != null) {
                Location s = currentLocation;
                s.setLay(2);
                switch (k.toLowerCase()) {

                    case "up":
                        if (gr.get(s.getAdjacentLocation(Location.NORTH)) instanceof Door) {
                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.NORTH));
                            d.open();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    case "down":
                        if (gr.get(s.getAdjacentLocation(Location.SOUTH)) instanceof Door) {

                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.SOUTH));
                            d.open();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    case "left":
                        if (gr.get(s.getAdjacentLocation(Location.WEST)) instanceof Door) {

                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.WEST));
                            d.open();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    case "right":
                        if (gr.get(s.getAdjacentLocation(Location.EAST)) instanceof Door) {

                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.EAST));
                            d.open();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    default:
                        MainFrame.console.append("That is not a direction");
                }
                open = false;
            }
        } else if (close) {
            debug.println("it isnt working");
            if (gr != null) {
                Location s = currentLocation;
                s.setLay(2);
                switch (k.toLowerCase()) {

                    case "up":
                        if (gr.get(s.getAdjacentLocation(Location.NORTH)) instanceof Door) {
                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.NORTH));
                            d.close();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    case "down":
                        if (gr.get(s.getAdjacentLocation(Location.SOUTH)) instanceof Door) {

                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.SOUTH));
                            d.close();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    case "left":
                        if (gr.get(s.getAdjacentLocation(Location.WEST)) instanceof Door) {

                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.WEST));
                            d.close();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    case "right":
                        if (gr.get(s.getAdjacentLocation(Location.EAST)) instanceof Door) {

                            Door d = (Door) gr.get(s.getAdjacentLocation(Location.EAST));
                            d.close();
                        } else {
                            MainFrame.console.append("There is nothing to open");
                        }
                        break;
                    default:
                        MainFrame.console.append("That is not a direction");
                }
            }
            close = false;

        } else {
            switch (k) {
                case "UP":
                    setDirection(Location.NORTH);
                    move();
                    break;

                case "DOWN":
                    setDirection(Location.SOUTH);
                    move();
                    break;
                //END DOWN

                case "LEFT":
                    setDirection(Location.WEST);
                    move();
                    break;
                //END LEFT

                case "RIGHT":
                    setDirection(Location.EAST);
                    move();
                    break;
                //END RIGHT

                case "WIELD_RIGHT":

                    break;
                //END WIELD_RIGHT

                case "EQUIP_CHEST":
                    break;
                //END EQUIP_CHEST

                case "PICK_UP":
                    currentLocation.setLay(1);

                    if (gr.get(currentLocation) instanceof Item) {
                        Item ground = (Item) gr.get(currentLocation);

                        inventory.add(ground);
                        ground.removeSelfFromGrid();
                        ground.setLocation(null);

                        GameWindow.gamePanel.repaint();
                        GameWindow.gamePanel.revalidate();

                        MainFrame.console.append(ground.toString() + " added to inventory");
                    }

                    break;
                //END PICK_UP

                case "DROP":
                    break;
                //END DROP

                case "INVENTORY":
                    if (!MainFrame.inv.isVisible()) {
                        // RunGame.frame.remove(MainFrame.window);

                        RunGame.frame.add(MainFrame.inv, BorderLayout.CENTER);
                        MainFrame.inv.setVisible(true);
                        MainFrame.window.setVisible(false);
                        RunGame.frame.revalidate();
                        MainFrame.inv.revalidate();

                        debug.println("TEST");
                    } else {

                        RunGame.frame.add(MainFrame.window, BorderLayout.CENTER);

                        MainFrame.inv.setVisible(false);
                        MainFrame.window.setVisible(true);
                    }

                    break;
                //END INVENTORY

                case "QUIT_S":
                    q = new QuitGame();
                    q.setVisible(true);
                    RunGame.frame1.add(q);
                    RunGame.frame.setVisible(false);

                    RunGame.frame1.repaint();
                    RunGame.frame1.revalidate();
                    break;
                //END QUIT_S

                case "PASS_TURN":
                    passTurn();
                    break;
                //END PASS_TURN

                case "HEAL":
                    while (getCurrentHealth() < getMaxHealth() && !pollMob()) {
                        passTurn();
                    }
                    if (getCurrentHealth() == getMaxHealth()) {
                        MainFrame.console.append(getName() + " is fully healed.");
                    }
                    if (pollMob()) {
                        MainFrame.console.append(getName() + " can't rest with enemies so near!");
                    }

                    break;
                //END HEAL

                case "QUIT_NS":
                    q = new QuitGame();
                    q.setVisible(true);
                    RunGame.frame1.add(q);
                    RunGame.frame.setVisible(false);

                    RunGame.frame1.repaint();
                    RunGame.frame1.revalidate();

                    break;
                //END QUIT_NS

                case "GO_UP":
                    //go upstairs if standing on a "StairsUp" terrain
                    break;
                //END GO_UP

                case "GO_DOWN":
                    //go downstairs if standing on a "StairsDown" terrain
                    break;
                //END GO_DOWN

                case "QUERY":
                    //list controls
                    if (!MainFrame.cPane.isVisible()) {
                        // RunGame.frame.remove(MainFrame.window);

                        RunGame.frame.add(MainFrame.cPane, BorderLayout.CENTER);
                        MainFrame.cPane.setVisible(true);
                        MainFrame.window.setVisible(false);
                        RunGame.frame.revalidate();
                        MainFrame.cPane.revalidate();

                        debug.println("TEST");
                    } else {

                        RunGame.frame.add(MainFrame.window, BorderLayout.CENTER);

                        MainFrame.cPane.setVisible(false);
                        MainFrame.window.setVisible(true);
                    }
                    break;
                //END QUERY

                case "CLOSE":
                    MainFrame.console.append("Which direction should " + getName() + " close");

                    close = true;
                    break;
                //END CLOSE

                case "OPEN":
                    MainFrame.console.append("Which direction should " + getName() + " open");

                    open = true;
                    break;
                //END UP

            }
        }

        if (!k.equalsIgnoreCase("INVENTORY") && !k.equalsIgnoreCase("QUERY")) {
            GameWindow.actAll();
            RunGame.score++;
        }
        if (this.isDead()) {
            MainFrame.console.append(this.getName() + " is dead; Game Over.");

            GameOver g = new GameOver();
            g.setVisible(true);
            RunGame.frame1.add(g);
            RunGame.frame.setVisible(false);

            RunGame.frame1.repaint();
            RunGame.frame1.revalidate();
        }
    }

    private boolean pollMob() {
        int x = this.getLocation().getCol(),
                y = this.getLocation().getRow();
        Grid gr = getGrid();
        ArrayList neighbors = gr.getNeighbors(new Location(y, x, 0));
        boolean mob = false;

        return !neighbors.isEmpty();
    }

    @Override
    public void move() {
        Grid<Entity> gr = getGrid();
        Location loc = this.getLocation().getAdjacentLocation(getDirection());
        loc.setLay(0);
        if (gr.isValid(loc) && canMove()) {
            moveTo(loc);
        }
        loc.setLay(2);
        if (gr.isValid(loc) && gr.get(loc) instanceof Door) {
            Door d = (Door) gr.get(loc);
            if (d.getCollision()) {
                d.open();
            } else if(!(gr.get(new Location(loc.getCol(),loc.getRow(),0)) instanceof MOB)){
                MainFrame.console.append(getName() + " passes through the doorway");
            }
        }
        loc.setLay(1);
        if ((Item) gr.get(loc) instanceof Item && gr.isValid(loc)) {
            Item ground = (Item) gr.get(loc);
            MainFrame.console.append("A " + ground.toString() + " lays beneath " + getName() + "'s feet");
        }
        RunGame.frame.getInfo().update();
    }

    public ArrayList<Item> getInv() {
        return inventory;
    }

    public void equip(Item get) {
        if (get instanceof Armor) {
            Armor armor = (Armor) get;
            setArmor(armor);
            MainFrame.console.append(armor.toString() + " was equipped");
        } else if (get instanceof Weapon) {
            Weapon weapon = (Weapon) get;
            setWeapon(weapon);
            MainFrame.console.append(weapon.toString() + " was equipped");
        }
        RunGame.frame.getInfo().update();
    }
}
