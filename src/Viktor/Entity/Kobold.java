/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Kobold.java
 *
 * Description: Weak low level MOB. Common opponent for the player
 */
package Viktor.Entity;

public class Kobold extends MOB{
    public Kobold(){
        setAwareness(4);
        setWeapon(new Weapon(1,1));
        setArmor(new Armor(0,0));
        setName("Kobold");
        setStrength(5);
        setDexterity(10);
        setConstitution(7);
        setAgility(13);
        setMaxHealth(20 + getStrength() + (int) Math.floor(getConstitution() * 1.2));
        setCurrentHealth(getMaxHealth());
    }
    
    public Kobold(int x, int y){
        this();
        setLocation(x,y,0);
    }
}
