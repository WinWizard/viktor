/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Minotaur.java
 *
 * Description: High Level MOB, has a small chance to spawn and can destroy doors
 */

package Viktor.Entity;

import Viktor.GUI.GameWindow;
import static Viktor.GUI.GameWindow.gamePanel;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

public class Minotaur extends MOB{
    public Minotaur(){
        setAwareness(5);
        setWeapon(new Weapon(2,2));
        setArmor(new Armor(2,0));
        setName("Minotaur");
        setStrength(16);
        setDexterity(5);
        setConstitution(16);
        setAgility(12);
        setMaxHealth(20 + getStrength() + (int) Math.floor(getConstitution() * 1.2));
        setCurrentHealth(getMaxHealth());
    }
    
    public Minotaur(int x, int y){
        this();
        setLocation(x,y,0);
    }
    
    @Override
    @SuppressWarnings("empty-statement")
    public boolean canMove() {
        Location loc = this.getLocation().getAdjacentLocation(getDirection());

        Grid gr = getGrid();
        boolean move = true;
        Entity e;
        for (int i = 0; i < gr.getNumLays() && move; i++) {
            loc.setLay(i);

            e = (Entity) gr.get(loc);
            if (e != null) {
                if (i == 0 && ((this instanceof MOB && e instanceof Player))) {
                    Character t = (Character) gr.get(loc);
                    this.attack(t);
                    move = false;
                } else if(gr.get(loc) instanceof Door);
                    else if (e.getCollision()) {
                    //if(gr.get(loc) instanceof Character || gr.get(loc) instanceof Wall || (gr.get(loc) instanceof Door || !gr.get(loc){
                    move = false;
                }
            }
        }
        return move;
    }
    
    @Override
    public void moveTo(Location newLocation) {
        Grid<Entity> gr = getGrid();
        this.getLocation().setLay(0);
        if (gr == null) {
            throw new IllegalStateException("This actor is not in a grid.");
        }
        if (gr.get(this.getLocation()) != this) {
            throw new IllegalStateException(
                    "The grid contains a different actor at location "
                    + getLocation() + ".");
        }
        if (!gr.isValid(newLocation)) {
            throw new IllegalArgumentException("Location " + newLocation
                    + " is not valid.");
        }

        if (newLocation.equals(getLocation())) {
            return;
        }
        gr.remove(getLocation());
        Entity other = gr.get(newLocation);
        Location temp = new Location(newLocation.getRow(),newLocation.getCol(),2);
        if (gr.get(temp) instanceof Door && gr.get(temp).getCollision()) {
            gr.get(temp).removeSelfFromGrid();
            gamePanel.addOccupant(new Floor(newLocation.getRow(), newLocation.getCol()).getLocation(), new Floor(newLocation.getRow(), newLocation.getCol()));
        }
        this.setLocation(newLocation);
        gr.put(this.getLocation(), this);
        debug.println(gr.get(this.getLocation()).toString());
        GameWindow.gamePanel.repaint();
        GameWindow.gamePanel.revalidate();

    }
}
