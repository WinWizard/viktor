/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Terrain.java
 *
 * Description: subclass of entity that allows for collision and puts all terrain on layer 2
 */
package Viktor.Entity;

import info.gridworld.grid.Location;

public class Terrain extends Entity {
    
    public Terrain(){
        this(false,new Location(0,0,2));
    }
    
    public Terrain(boolean collision){
        this(collision,new Location(0,0,2));
    }
    
    public Terrain(Location loc){
        this(false,loc);
    }
    
    public Terrain(boolean collision, Location loc){
        super();
        super.setLocation(loc);
        super.setCollision(collision);
    }        
}
