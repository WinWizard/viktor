/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Armor.java
 *
 * Description: A defensive item. Allows for the player to have one of four tiers of defense
 */
package Viktor.Entity;

import info.gridworld.grid.Location;

public class Armor extends Item {

    private int material;//0-3 0=fleshy, 1=wood 2=steel 3=mythril
    private int type;//0 0=chest
    //private booolean isBroken; //to be implemented. some form of durability/repair

    public Armor() { //default is (0,0,null)
        this(0);
    }

    public Armor(int aLvl) {
        this(aLvl, 0);
    }

    public Armor(int aLvl, int aType) {
        this(aLvl, aType, null);
    }

    public Armor(Location loc) {
        this(0, 0, loc);
    }

    public Armor(int aLvl, int aType, Location loc) {
        super(loc);
        material = aLvl;
        type = aType;

        grabImage();
    }

    public Armor(String armorMaterial, String armorType, Location loc) {
        super(loc);
        switch (armorMaterial.toLowerCase()) {
            case "bare":
                material = 0;
                break;
            case "wooden":
                material = 1;
                break;
            case "steel":
                material = 2;
                break;
            case "mythril":
                material = 3;
                break;
            default:
                material = 0;
        }
        switch (armorType.toLowerCase()) {
            case "chest":
                type = 0;
            default:
                type = 0;
        }
        
        grabImage();
    }

    public Armor(String armorMaterial, String armorType) {
        this(armorMaterial, armorType, null);
    }

    public boolean setMaterial(String aLvl) {
        switch (aLvl.toLowerCase()) {
            case "bare":
                material = 0;
                break;
            case "wooden":
                material = 1;
                break;
            case "steel":
                material = 2;
                break;
            case "mythril":
                material = 3;
                break;
            default:
                return false;
        }
        return true;
    }

    public void setMaterial(int aLvl) {
        material = aLvl;
    }

    public int getMaterial() {
        return material;
    }

    public boolean setArmorType(String aType) {
        switch (aType.toLowerCase()) {
            case "chest":
                type = 0;
                break;
            default:
                return false;
        }
        return true;
    }

    public void setArmorType(int aType) {
        type = aType;
    }

    public int getArmorType() {
        return type;
    }

    public String getMaterialName() {
        switch (material) {
            case 0:
                return "Bare";
            case 1:
                return "Wooden";
            case 2:
                return "Steel";
            case 3:
                return "Mythril";
            default:
                return "unknown material";

        }
    }

    public String getArmorName() {
        switch (type) {
            case 0:
                return "Chest";
            default:
                return "unknown material";
        }
    }

    @Override
    public String toString() {
        return (getMaterialName() + " "+getArmorName());
    }
    
    private void grabImage() {
        switch (material) {
            case 1://woooden
                switch (type) {
                    case 0:
                        setImage("ArmorWooden");
                        break;
                }
                //end switch(type)
                break;
            //end material: wood
            case 2:
                switch (type) {
                    case 0://steel
                        setImage("ArmorSteel");
                        break;
                }
                //end switch(type)
                break;
            //end material: steel
            case 3:
                switch (type) {
                    case 0://mythril
                        setImage("ArmorMythril");
                        break;
                }
                //end switch(type)
                break;
            //end material: mythril
        }//end switch(material)
    }
}
