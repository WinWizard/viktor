/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: MOB.java
 *
 * Description: subclass of character that contains the basic AI for enemy characters
 */
package Viktor.Entity;

import Viktor.GUI.MainFrame;
import Viktor.RunGame;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

/**
 *
 * @author zach
 */
public class MOB extends Character {
    private boolean playerFound;
    private int awareness;
    
    public MOB(){
        playerFound = false;
        awareness = 3;
    }
    
    public MOB(int x, int y){
        this();
        setLocation(x,y,0);
    }
    
    public int getAwareness(){
        return awareness;
    }
    
    public void setAwareness(int a){
        awareness = a;
    }
    
    @Override
    public void act(){
        if(this.isDead()){
            MainFrame.console.append(this.getName() + " has died.");
        }
        else{
            if(!playerFound){
                this.pollPlayer();
            }
            if(playerFound){
                this.move();
            }
        }
    }
    
    public void pollPlayer(){
        int x = this.getLocation().getCol(),
            y = this.getLocation().getRow();
        Location l = RunGame.player.getLocation();
        if(l.getCol() >= x-awareness && l.getCol() <= x+awareness)
            if(l.getRow() >= y-awareness && l.getRow() <= y+awareness)
                playerFound = true;
    }
    
    @Override
    @SuppressWarnings("empty-statement")
    public void move(){
        Grid gr = getGrid();
        
         double y = this.getLocation().getRow() - RunGame.player.getLocation().getRow(),
                x = RunGame.player.getLocation().getCol() - this.getLocation().getCol(),
                theta = 0;
         if(x != 0){
             theta = Math.atan(y/x);
             if(x < 0){
                 if(y >= 0)
                    theta = Math.PI+theta;
                 else
                     theta = -Math.PI+theta;
             }
         }
         if(x == 0){
             if(y > 0)
                 theta = Math.PI/2;
             else
                 theta = -Math.PI/2;
         }
         
         //octant 5
         if(theta >= -Math.PI && theta < -Math.PI*3/4){
             setDirection(Location.WEST);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.SOUTH);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.NORTH);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.EAST);
                        step();
                    }
                }
             }
         }
         
         //octant 6
         if(theta >= -Math.PI*3/4 && theta < -Math.PI/2){
             setDirection(Location.SOUTH);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.WEST);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.EAST);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.NORTH);
                        step();
                    }
                }
             }
         }
         
         //octant 7
         if(theta >= -Math.PI/2 && theta < -Math.PI/4){
             setDirection(Location.SOUTH);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.EAST);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.WEST);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.NORTH);
                        step();
                    }
                }
             }
         }
         
         //octant 8
         if(theta >= -Math.PI/4 && theta < 0){
             setDirection(Location.EAST);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.SOUTH);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.NORTH);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.WEST);
                        step();
                    }
                }
             }
         }
         
         //octant 1
         if(theta >= 0 && theta < Math.PI/4){
             setDirection(Location.EAST);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.NORTH);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.SOUTH);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.WEST);
                        step();
                    }
                }
             }
         }
         
         //octant 2
         if(theta >= Math.PI/4 && theta < Math.PI/2){
             setDirection(Location.NORTH);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.EAST);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.WEST);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.SOUTH);
                        step();
                    }
                }
             }
         }
         
         //octant 3
         if(theta >= Math.PI/2 && theta < Math.PI*3/4){
             setDirection(Location.NORTH);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.WEST);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.EAST);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.SOUTH);
                        step();
                    }
                }
             }
         }
         
         //octant 4
         if(theta >= Math.PI*3/4 && theta <= Math.PI){
             setDirection(Location.WEST);
             if(canMove()){
                 step();
             }
             else if(gr.get(this.getLocation().getAdjacentLocation(this.getDirection())) instanceof Player);
             else{
                setDirection(Location.NORTH);
                if(canMove()){
                    step();
                }
                else{
                    setDirection(Location.SOUTH);
                    if(canMove()){
                        step();
                    }
                    else{
                        setDirection(Location.EAST);
                        step();
                    }
                }
             }
         }
    }
    
    public void step(){
        Grid<Entity> gr = getGrid();
        Location loc = this.getLocation().getAdjacentLocation(getDirection());
        loc.setLay(0);
        if(gr.isValid(loc) && canMove()){
            moveTo(loc);
        }
    }
}
