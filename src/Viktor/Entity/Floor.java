/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Floor.java
 *
 * Description: Standard non-collidable Terrain entity for MOBs and the player to traverse 
 */
package Viktor.Entity;

import info.gridworld.grid.Location;

public class Floor extends Terrain {

    public Floor() {
        super(false);
    }
    
    public Floor(Location loc){
        super(false,loc);
    }
    public Floor(int x, int y){
        super(false, new Location(x,y,2));
    }
}
