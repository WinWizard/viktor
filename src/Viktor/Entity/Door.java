/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Door.java
 *
 * Description: A Terrain that can toggle weather its collidable
 */
package Viktor.Entity;

import Viktor.GUI.GameWindow;
import Viktor.GUI.MainFrame;
import info.gridworld.grid.Location;

public class Door extends Terrain {

    public Door() {
        super(true);
    }

    public Door(Location loc) {
        super(true, loc);
    }

    public Door(int x, int y) {
        super(true, new Location(x, y, 2));
    }

    public boolean open() {
        super.setCollision(false);
        super.setImage("DoorOpen");
        GameWindow.gamePanel.repaint();
        GameWindow.gamePanel.revalidate();

        MainFrame.console.append("The door is now open");

        return true;
    }

    public boolean close() {
        super.setCollision(true);
        super.setImage("Door");
        GameWindow.gamePanel.repaint();
        GameWindow.gamePanel.revalidate();

        MainFrame.console.append("The door is now closed");

        return true;
    }
}
