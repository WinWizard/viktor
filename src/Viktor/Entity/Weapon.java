/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Weapon.java
 *
 * Description:  An offensive item. Allows for the player to have one of 4 tiers of offense
 */
package Viktor.Entity;

import info.gridworld.grid.Location;

public class Weapon extends Item {

    private int material;//0-3 0=fleshy, 1=wood 2=steel 3=mythril
    private int type;//0-3 0=fist 1=sword 2=axe 3=mace
    //private booolean isBroken; //to be implemented. some form of durability/repair

    public Weapon() { //default is (0,0,null)
        this(0);
    }

    public Weapon(int wLvl) {
        this(wLvl, 0);
    }

    public Weapon(int wLvl, int wType) {
        this(wLvl, wType, null);
    }

    public Weapon(Location loc) {
        this(0, 0, loc);
    }

    public Weapon(int wLvl, int wType, Location loc) {
        super(loc);
        material = wLvl;
        type = wType;

        grabImage();
    }

    public Weapon(String wLvl, String wType, Location loc) {
        super(loc);

        switch (wLvl.toLowerCase()) {
            case "fleshy":
                material = 0;
                break;
            case "wooden":
                material = 1;
                break;
            case "steel":
                material = 2;
                break;
            case "mythril":
                material = 3;
                break;
            default:
                material = 0;
        }
        switch (wType.toLowerCase()) {
            case "fist":
                type = 0;
                break;
            case "sword":
                type = 1;
                break;
            case "axe":
                type = 2;
                break;
            case "mace":
                type = 3;
                break;
            default:
                type = 0;
        }

        grabImage();

    }

    public Weapon(String wLvl, String wType) {
        this(wLvl, wType, null);
    }

    public boolean setMaterial(String wLvl) {
        switch (wLvl.toLowerCase()) {
            case "fist":
                material = 0;
                break;
            case "wooden":
                material = 1;
                break;
            case "steel":
                material = 2;
                break;
            case "mythril":
                material = 3;
                break;
            default:
                return false;
        }
        return true;
    }

    public void setMaterial(int wLvl) {
        material = wLvl;
    }

    public int getMaterial() {
        return material;
    }

    public boolean setWeaponType(String wType) {
        switch (wType.toLowerCase()) {
            case "fist":
                type = 0;
                break;
            case "sword":
                type = 1;
                break;
            case "axe":
                type = 2;
                break;
            case "mace":
                type = 3;
                break;
            default:
                return false;
        }
        return true;
    }

    public void setWeaponType(int wType) {
        type = wType;
    }

    public int getWeaponType() {
        return type;
    }

    public String getMaterialName() {
        switch (material) {
            case 0:
                return "Fleshy";
            case 1:
                return "Wooden";
            case 2:
                return "Steel";
            case 3:
                return "Mythril";
            default:
                return "Unknown Material";

        }
    }

    public String getWeaponName() {
        switch (type) {
            case 0:
                return "Fist";
            case 1:
                return "Sword";
            case 2:
                return "Axe";
            case 3:
                return "Mace";
            default:
                return "Unknown Material";
        }
    }

    @Override
    public String toString() {
        return (getMaterialName() + " " + getWeaponName());
    }

    private void grabImage() {
        switch (material) {
            case 1://woooden
                switch (type) {
                    case 1:
                        setImage("SwordWooden");
                        break;
                    //end type: sword
                    case 2:
                        setImage("AxeWooden");
                        break;
                    //end type: axe
                    case 3:
                        setImage("MaceWooden");
                        break;
                    //end type: mace
                }
                //end switch(type)
                break;
            //end material: wood
            case 2:
                switch (type) {
                    case 1://steel
                        setImage("SwordSteel");
                        break;
                    //end type: sword
                    case 2:
                        setImage("AxeSteel");
                        break;
                    //end type: axe
                    case 3:
                        setImage("MaceSteel");
                        break;
                    //end type: mace
                }
                //end switch(type)
                break;
            //end material: steel
            case 3:
                switch (type) {
                    case 1://mythril
                        setImage("SwordMythril");
                        break;
                    //end type: sword
                    case 2:
                        setImage("AxeMythril");
                        break;
                    //end type: axe
                    case 3:
                        setImage("MaceMythril");
                        break;
                    //end type: mace
                }
                //end switch(type)
                break;
                //end material: mythril

        }//end switch(material)
    }
}
