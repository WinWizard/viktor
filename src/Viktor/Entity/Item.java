/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Item.java
 *
 * Description: subclass of entity allowing for further specialization into Weapon and Armor
 */
package Viktor.Entity;

import info.gridworld.grid.Location;


public class Item extends Entity {
    public Item(){
        this(null);
    }
    public Item(Location loc){ // if loc is null then item is in inventory
        super();
        super.setCollision(false);
        super.setLocation(loc);
    }
    
}
