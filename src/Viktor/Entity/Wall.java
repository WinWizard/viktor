/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Wall.java
 *
 * Description: Standard collidable entity that keeps characters within the map
 */
package Viktor.Entity;

import info.gridworld.grid.Location;

public class Wall extends Terrain {

    public Wall() {
        super(true);
    }
    
    public Wall(Location loc){
        super(true,loc);
    }
    
    public Wall(int x, int y){
        super(true, new Location(x,y,2));
    }
}
