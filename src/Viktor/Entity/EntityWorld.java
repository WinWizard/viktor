/*
 *Implemented from Gridworld
 */
package Viktor.Entity;

import Viktor.Debugger;
import Viktor.RunGame;
import Viktor.World.World;
import info.gridworld.grid.BoundedGrid;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * An <code>EntityWorld</code> is occupied by entitys. <br />
 * This class is not tested on the AP CS A and AB exams.
 */
public class EntityWorld extends World<Entity> {

    int rowStart, colStart;

    private static final int DEFAULT_ROWS = 30;
    private static final int DEFAULT_COLS = 40;
    private static final int DEFAULT_LAYS = 3;
    Debugger debug = RunGame.debug;
    private static final String DEFAULT_MESSAGE = "Click on a grid location to construct or manipulate an entity.";

    /**
     * Constructs an entity world with a default grid.
     */
    public EntityWorld() {

        super(new BoundedGrid<Entity>(DEFAULT_ROWS, DEFAULT_COLS, DEFAULT_LAYS));
        //ddOccupant(RunGame.player.getLocation(), RunGame.player);
        this.setBackground(Color.BLACK);

        setLayout(new GridBagLayout());
        rowStart = colStart = 0;

    }

    /**
     * Constructs an entity world with a given grid.
     *
     * @param grid the grid for this world.
     */
    public EntityWorld(Grid<Entity> grid) {
        super(grid);
    }

    public void show() {
        if (getMessage() == null) {
            setMessage(DEFAULT_MESSAGE);
        }
        super.show();
    }

    public void step() {
        Grid<Entity> gr = getGrid();
        ArrayList<Entity> entitys = new ArrayList<Entity>();
        for (Location loc : gr.getOccupiedLocations()) {
            entitys.add(gr.get(loc));
        }

        for (Entity a : entitys) {
            // only act if another entity hasn't removed a
            if (a.getGrid() == gr) {
                a.act();
            }
        }
    }

    /**
     * Adds an entity to this world at a given location.
     *
     * @param loc the location at which to add the entity
     * @param occupant the entity to add
     */
    @Override
    public void addOccupant(Location loc, Entity occupant) {
        occupant.putSelfInGrid(getGrid(), loc);

        repaint();
        revalidate();

    }

    /**
     * Adds an occupant at a random empty location.
     *
     * @param occupant the occupant to add
     */
    /**
     * Removes an entity from this world.
     *
     * @param loc the location from which to remove an entity
     * @return the removed entity, or null if there was no entity at the given
     * location.
     */
    public Entity removeOccupant(Location loc) {
        Entity occupant = getGrid().get(loc);
        if (occupant == null) {
            return null;
        }
        occupant.removeSelfFromGrid();
        repaint();

        return occupant;
    }
    /*
     Repaints the grid with all entities
    
     */

    @Override
    public void repaint() {
        this.removeAll(); //remove everything from the grid befor adding them back

        Grid<Entity> gr = RunGame.player.getGrid();//get the grid from the player

        GridBagConstraints gbc = new GridBagConstraints(); //create a contraints object for the layout

        gbc.gridheight = 1;
        gbc.gridwidth = 1;

        BufferedImage img;//create a temporary image for image compositing

        Entity ent = null;//create a container entity
        JLabel temp = new JLabel();//create a jlabel to add
        temp.setBackground(Color.BLACK);
        Graphics g;
        if (gr != null) {//make sure grid exists

            for (int i = rowStart, j = colStart, l = 0; i < rowStart + 30 && j < colStart + 40; l++) {

                img = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);//create an image to contain the composite
                g = img.getGraphics(); //point g to the graphics component of the image
                temp = new JLabel();//reset temp
                temp.setBackground(Color.BLACK);

                ent = gr.get(new Location(i, j, gr.getNumLays() - 1));//get the first entity from the grid

                for (int k = gr.getNumLays() - 1; k >= 0;) {//loop through the array  to add occupants
                    if (ent != null) {//if there is an actor at the location add the image to the temporary file

                        g.drawImage(ent.getImage(), 0, 0, null);
                    }

                    k--;
                    if (k >= 0) {//if the layers have all been read set ent to the next location
                        ent = gr.get(new Location(i, j, k));
                    }

                }
                temp = new JLabel(new ImageIcon(img));//create a jLabel of the image

                temp.setBackground(Color.BLACK);

                gbc.gridx = j;
                gbc.gridy = i;
                this.add(temp, gbc); //add the label to the window at the coordinates (i,j)

                i++;
                if (i == rowStart + 30) {
                    i = 0;
                    j++;
                }

            }
        }

        // super.repaint();
    }

   //*/
}
