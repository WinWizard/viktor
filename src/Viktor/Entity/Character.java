/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Character.java
 *
 * Description: Highest level class for players and MOBs.
 */
package Viktor.Entity;

import static Viktor.GUI.GameWindow.actAll;
import Viktor.GUI.MainFrame;
import Viktor.RunGame;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

public class Character extends Entity {

    private String name;
    private int strength, dexterity, constitution, agility, wisdom, luck, currentHealth, maxHealth, healTick;
    private double accuracy, dodge, atkPower, defPower;
    private Weapon weapon;
    private Armor armor;

    public Character() {
        this(null);
    }

    public Character(Location loc) {
        super();
        super.setCollision(true);
        super.setLocation(loc);
        healTick = 0;
        strength = 10;
        dexterity = 10;
        constitution = 10;
        agility = 10;
        maxHealth = 20 + strength + (int) Math.floor(constitution * 1.2);
        currentHealth = maxHealth;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int str) {
        strength = str;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dex) {
        dexterity = dex;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int con) {
        constitution = con;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wis) {
        wisdom = wis;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agi) {
        agility = agi;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int lck) {
        luck = lck;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    //you cannot die from loss of maxHealth unless it goes to 0
    public void setMaxHealth(int mxHealth) {
        currentHealth += mxHealth - maxHealth;
        if (mxHealth <= 0) {
            maxHealth = 0;
            currentHealth = 0;
        } else {
            if (currentHealth <= 0) {
                currentHealth = 1;
            }
            maxHealth = mxHealth;
        }
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int cHealth) {
        if (cHealth < 0) {
            currentHealth = 0;
        } else if (cHealth > maxHealth) {
            currentHealth = maxHealth;
        } else {
            currentHealth = cHealth;
        }
    }

    public void calcStats() {
        double[] mult = new double[2];
        if(weapon.getWeaponType() == 0){
            mult[0] = .9;
            mult[1] = .9;
        }
        else if(weapon.getWeaponType() == 1){
            mult[0] = 1.1;
            mult[1] = .9;
        }
        else if(weapon.getWeaponType() == 2){
            mult[0] = 1.0;
            mult[1] = 1.0;
        }
        else{
            mult[0] = .9;
            mult[1] = 1.1;
        }
        double rnd = Math.random() / 2 - .25;
        accuracy = dexterity * Math.pow(1 + weapon.getMaterial(), .5) * mult[0];
        dodge = agility * Math.pow(1 + armor.getMaterial(), .5);
        atkPower = strength * Math.pow(2 * (1 + weapon.getMaterial()) / 3, .5) * (1 + rnd) * mult[1];
        defPower = constitution * Math.pow(2 * (1 + armor.getMaterial()) / 3, .5) * (1 + rnd);
    }

    public double getAccuracy() {
        return accuracy;
    }

    public double getDodge() {
        return dodge;
    }

    public double getAtkPower() {
        return atkPower;
    }

    public double getDefPower() {
        return defPower;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon next) {
        weapon = next;
    }

    public Armor getArmor() {
        return armor;
    }

    public void modifyArmor() {
        //to be implemented
    }

    public void setArmor(Armor next) {
        armor = next;
    }

    public String getName() {
        return name;
    }

    public void setName(String title) {
        name = title;
    }

    public void move() {
        Grid<Entity> gr = getGrid();
        Location loc = this.getLocation().getAdjacentLocation(getDirection());
        if (gr.isValid(loc) && canMove()) {
            moveTo(loc);
        }
    }

    public void attack(Character target) {
        this.calcStats();
        target.calcStats();
        double toHit = Math.random() * 100, hitChance, hitRatio;
        int damage;

        if (target.getDodge() < 1) {
            hitRatio = this.getAccuracy() / target.getDodge();
        } else {
            hitRatio = this.getAccuracy();
        }

        hitChance = Character.calcHitChance(hitRatio);

        if (hitChance >= toHit) {
            damage = Character.damageDealt(this, target);
            target.setCurrentHealth(target.getCurrentHealth() - damage);
            MainFrame.console.append(this.getName() + " hit " + target.getName() + " for " + damage + ".");
        } else {
            MainFrame.console.append(this.getName() + " misses " + target.getName() + ".");
        }
        RunGame.frame.getInfo().update();
    }

    private static int damageDealt(Character atk, Character def) {
        double damage = 1;
        if (atk.getAtkPower() >= 2 * def.getDefPower()) {
            damage = 1.5 * atk.getAtkPower();
        } else if (atk.getAtkPower() < 2 * def.getDefPower() && atk.getAtkPower() >= def.getDefPower()) {
            damage = atk.getAtkPower();
        } else if (atk.getAtkPower() < def.getDefPower() && atk.getAtkPower() >= .5 * def.getDefPower()) {
            damage = atk.getAtkPower() / 2;
        } else {
            damage = atk.getAtkPower() / 4;
        }
        int dmg = (int) Math.round(damage);
        if (dmg < 1) {
            dmg = 1;
        }
        return dmg;
    }

    private static double calcHitChance(double hitRatio) {
        if (hitRatio >= 4) {
            return 95;
        } else if (hitRatio > 4 && hitRatio >= 3) {
            return (hitRatio - 3) * 7.5 + 87.5;
        } else if (hitRatio > 3 && hitRatio >= 2) {
            return (hitRatio - 2) * 12.5 + 75;
        } else if (hitRatio > 2 && hitRatio >= 1) {
            return (hitRatio - 1) * 25 + 50;
        }
        return hitRatio * 35 + 15;
    }

    public boolean canMove() {
        Location loc = this.getLocation().getAdjacentLocation(getDirection());

        Grid gr = getGrid();
        boolean move = true;
        Entity e;
        for (int i = 0; i < gr.getNumLays() && move; i++) {
            loc.setLay(i);

            e = (Entity) gr.get(loc);
            if (e != null) {
                if (i == 0 && ((this instanceof Player && e instanceof MOB) || (this instanceof MOB && e instanceof Player))) {
                    Character t = (Character) gr.get(loc);
                    this.attack(t);
                    move = false;
                } else if (e.getCollision()) {
                    //if(gr.get(loc) instanceof Character || gr.get(loc) instanceof Wall || (gr.get(loc) instanceof Door || !gr.get(loc){
                    move = false;
                }
            }

        }
        return move;
    }

    public void passTurn() {
        if(++healTick > 2){
            setCurrentHealth(getCurrentHealth()+1);
            healTick = 0;
            RunGame.frame.getInfo().update();
        }
        actAll();
        RunGame.score++;
    }
    
    public boolean isDead(){
        return currentHealth == 0;
    }
}
