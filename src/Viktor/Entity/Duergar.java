/*
 * Zachary McMinn, Dylan Kozicki, Ryan Ousey
 * 
 * Last Revised: 12/15/14
 * 
 * File Name: Duergar.java
 *
 * Description: An enemy that is tougher than the Kobold but not the Minotaur
 */

package Viktor.Entity;

public class Duergar extends MOB{
    public Duergar(){
        setAwareness(4);
        setWeapon(new Weapon(2,3));
        setArmor(new Armor(1,0));
        setName("Minotaur");
        setStrength(11);
        setDexterity(8);
        setConstitution(14);
        setAgility(7);
        setMaxHealth(20 + getStrength() + (int) Math.floor(getConstitution() * 1.2));
        setCurrentHealth(getMaxHealth());
    }
    
    public Duergar(int x, int y){
        this();
        setLocation(x,y,0);
    }
}
